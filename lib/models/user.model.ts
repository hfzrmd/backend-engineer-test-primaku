import {Sequelize, Model, DataTypes, BuildOptions} from "sequelize";
import {database} from "../config/database";
import * as bcrypt from 'bcrypt';

export class Users extends Model {
  public id!: number;
  public name!: string;
  public role!: string;
  public email!: string;
  public password!: string;
}

Users.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(35),
      allowNull: false,
    },
    role: {
      type: DataTypes.ENUM('admin', 'user'),
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING(35),
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataTypes.STRING(100),
      allowNull: false,
    }
  },
  {
    tableName: "users",
    sequelize: database,
    freezeTableName: true,
  }
  )
  
  Users.sync({force: true}).then(() => console.log("Users table created"));
  Users.beforeCreate((user) => {
    return bcrypt.hash(user.password, 8)
    .then((hash: string) => { user.password = hash; })
    .catch((err: Error) => { throw err; });
  });
  
  Users.beforeBulkCreate((users, options) => {
    for (const user of users) {
      const {password} = user;
      return bcrypt.hashSync(password, 8).then((hash: string) => user.password = hash);
    }
  })
  
  export type UserInterface = {
    name: string;
    role: string;
    email: string;
    password: string;
  }
  
  export interface UserInterfaceUpdatePassword {
    password: string;
  }
  
  export interface UsersId {
    id: number;
  }
  