import {Request, Response, NextFunction} from "express";
import {IncomingHttpHeaders} from "http";
import {UserController} from "../controllers/user.controller";
import {Users} from '../models/user.model';
import * as jwt from "jsonwebtoken";

export class Routes {
    
    private readonly salt = 12;
    private readonly secret = "s3cretz@2022:hfzrmd!";
    
    public userControl: UserController = new UserController();
    
    public routes(app): void {
        app.route("/").get(this.userControl.index);
        app.route("/api/users")
        .get(this.userControl.users)
        .post(this.userControl.create);
        
        app.route('/api/users/:id')
        .get(this.userControl.find)
        .delete(this.userControl.delete)
        .patch(this.userControl.update);
        
        app.route('/api/login').post(this.userControl.login);
        app.route('/api/password/:id').put(this.userControl.updatePassword);
    }
    
    public verifyToken(token: string){
        return new Promise((resolve, reject) => {
            jwt.verify(token, this.secret, (err: Error, decoded) => {
                if (err) {
                    console.error(err);
                    resolve(false)
                    return false;
                }
                Users.findByPk(decoded['id'])
                resolve(true)
                return true
            })
        }) as Promise <boolean>
    }
    public getTokenFromHeader(headers: IncomingHttpHeaders): string{
        const header = headers.authorization as string
        if (!header) {
            return header;
        }
        return header.split(' ')[1]
    }
    public Guard(req: Request, res: Response, next: NextFunction){
        if (!req.header('Authorization')) {
            return res.status(403);
        }
        const token = this.getTokenFromHeader(req.headers);
        if (token) {
            const access = this.verifyToken(token)
            access.then(valid => {
                if (!valid) {
                    return res.status(401);                    
                }
                return next();
            })
        }else{
            return res.status(401);
        }
    }
}