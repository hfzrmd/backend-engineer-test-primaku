import {Request, Response} from "express";
import * as jwt from "jsonwebtoken";
import * as bcrypt from 'bcrypt';
import {DestroyOptions, UpdateOptions} from "sequelize";
import {Users, UserInterface, UserInterfaceUpdatePassword} from '../models/user.model';

export class UserController {

    private readonly salt = 12;
    private readonly secret = "s3cretz@2022:hfzrmd!";

    public index(req: Request, res: Response) {
        res.json("Hi, my name is Hafiz Ramadhan!")
    }
    public users(req: Request, res: Response) {
        Users.findAll<Users>({})
        .then((users: Array<Users>) => res.json(users))
        .catch((err: Error) => res.status(500).json(err))
    }
    public create(req: Request, res: Response) {
        if (Object.keys(req.body).length == 0) {
            return res.status(400).json({error: "bad request"});
        }
        const params = req.body;
        const {password, email} = req.body;
        const check = password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/);
        if (!check) {
            res.status(400).json("Please use at least lowercase, uppercase, one number and one special character");
        }
        const checkEmail = Users.findOne({
            where: {
                email: email
            }
        })
        if (checkEmail) {
            res.status(400).json("email already exist");
        }
        console.log(params);
        Users.create<Users>(params)
        .then((user: Users) => 
            res.status(201).json({
                "status": true,
                "message": "success",
                "data": user
            })
        )
        .catch((err: Error) => 
            res.status(500).json({
                "msg": err
            })
        )
    }
    public find(req: Request, res: Response) {
        const userId: number = Number(req.params.id);
        Users.findByPk<Users>(userId)
        .then((user: Users | null) => {
            if (user) {
              res.json(user);
            } else {
              res.status(404).json({errors: ["user not found"]});
            }
          })
          .catch((err: Error) => res.status(500).json(err));
    }
    public delete(req: Request, res: Response) {
        const userId: number = Number(req.params.id);
        const opt: DestroyOptions = {
            where: {
                id: userId,
                limit: 1,
            }
        };
        Users.destroy(opt)
        .then(() => res.status(200).json("success"))
        .catch((err: Error) => res.status(500).json(err));
    }
    public update(req: Request, res: Response) {
        // const {name, email, role} = req.body;
        if (Object.keys(req.body).length == 0) {
            return res.status(400).json({error: "bad request"});
        }
        const userId: number = Number(req.params.id);
        const params: UserInterface = req.body; // menyesuaikan kontrak
        const opt: UpdateOptions = {
            where: {
                id: userId,
                limit: 1,
            }
        };
        Users.update(params, opt)
        .then(() => res.status(200).json("success"))
        .catch((err: Error) => res.status(500).json(err))
    }
    public validatePassword(pass, hash): boolean {
        return bcrypt.compare(pass, hash);
    }
    public setToken(id: number, email: string, role: string): string {
        return jwt.sign(
            {
                id, email, role
            }, this.secret, {
                expiresIn: "1h"
            });
    }
    public login(req: Request, res: Response) {
        if (Object.keys(req.body).length == 0) {
            return res.status(400).json({error: "bad request"});
        }
        let token, validate;
        const email = req.body.email;
        const pwd = req.body.password;
        const find = Users.findOne({
            where: {
                email: email
            }
        })
        .then(user => {
            const {id, email, role, password} = user!
            validate = this.validatePassword(pwd, password)
            if (validate) {
                token = this.setToken(id, email, role);
            }
        })
        if (find && validate) {
            res.status(200).json({
                "status": true,
                "data": {
                    "token": token
                }
            })
        } else {
            res.status(404).json("data not found");
        }
    }
    public updatePassword(req: Request, res: Response) {
        if (Object.keys(req.body).length == 0) {
            return res.status(400).json({error: "bad request"});
        }
        const newpass = req.body.password;
        const check = newpass.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/);
        if (!check) {
            res.status(400).json("Please use at least lowercase, uppercase, one number and one special character");
        }
        const userId: number = Number(req.params.id);
        const params: UserInterfaceUpdatePassword = newpass; // menyesuaikan kontrak

        const opt: UpdateOptions = {
            where: {
                id: userId,
                limit: 1,
            }
        };
        Users.update(params, opt)
        .then(() => res.status(200).json("success"))
        .catch((err: Error) => res.status(500).json(err))
    }
}

function err(err: any): any {
    throw new Error("Function not implemented.");
}
