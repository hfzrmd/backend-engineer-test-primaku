'use strict';

const bcrypt = require('bcrypt');

const makePassword = (pw) => {
  return new Promise(async rs => {
    let salt, hash;
    salt = await bcrypt.genSalt(8);
    hash = await bcrypt.hash(pw, salt);
    return rs(hash);
  })
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      name: "admin",
      role: "admin",
      email:  "admin@primaku.com",
      password: await makePassword("#Admin123")
    }], {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
